import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManagerComponent} from '../manager/manager.component';

/**
 * Application routes.
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: '/manager',
    pathMatch: 'full'
  },
  {
    path: 'manager',
    component: ManagerComponent
  },
  {
    path: 'auth',
    loadChildren: () => import('../authentication/authentication.module').then(m => m.AuthenticationModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
