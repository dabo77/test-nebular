import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LoginContainerComponent} from './login-container/login-container.component';

/**
 * Routes within the authentication module.
 */
const routes: Routes = [
  {
    path: '',
    component: LoginContainerComponent,
    children: [
      {
        // need this dummy path to disable the navigation to /auth
        // because it is a nebular empty container
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
      },
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  }
];

/**
 * Responsible for the routing in the authentication module.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
