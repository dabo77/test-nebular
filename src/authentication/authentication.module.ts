import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {NbAuthModule} from '@nebular/auth';
import {AUTH_MODULE_CONFIG} from '../config/auth-module.config';
import {LoginComponent} from './login/login.component';
import {LoginContainerComponent} from './login-container/login-container.component';

/**
 * Responsible for the authentication related processes
 * (login, logout...).
 *
 * Uses nebular authentication module as its core.
 */
@NgModule({
  imports: [
    SharedModule,
    AuthenticationRoutingModule,
    NbAuthModule.forRoot(AUTH_MODULE_CONFIG),
  ],
  declarations: [
    LoginComponent,
    LoginContainerComponent
  ]
})
export class AuthenticationModule { }
