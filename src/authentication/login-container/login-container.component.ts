import {Component} from '@angular/core';
import {NbAuthComponent} from '@nebular/auth';

/**
 * Our custom authentication component
 * that overrides the nebular default authentication component
 * and holds f.e. the login component.
 *
 * (Customizes the template first of all.)
 */
@Component({
  templateUrl: 'login-container.component.html',
  styleUrls: ['login-container.component.scss']
})
export class LoginContainerComponent extends NbAuthComponent {

}
