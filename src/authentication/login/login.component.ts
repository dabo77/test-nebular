import {Component} from '@angular/core';
import {NbLoginComponent} from '@nebular/auth';

/**
 * Our custom login component
 * that overrides the nebular default login component.
 *
 * (Customizes the template first of all.)
 */
@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent extends NbLoginComponent {

}
