import { NbPasswordAuthStrategy } from '@nebular/auth';
import {environment} from '../environments/environment';

/**
 * Represents the configuration options object
 * for the nebular authentication module.
 */
export const AUTH_MODULE_CONFIG = {
  strategies: [
    NbPasswordAuthStrategy.setup({
      // name is the alias of exactly this strategy to mention it
      // but we can have more strategies (of the same type)
      name: 'email',

      // #### configure the authentication layer against our BE API

      // the actual base bend-point to the server
      // so this option defines, where the request will be sent
      baseEndpoint: environment.baseUrl,

      // define the api bend point to the server for the login (request)
      login: {
        // must match the actual BE api bend point
        endpoint: '/auth/login',
        // how we send the login request
        // post ist default but could be overridden to get
        method: 'post',
        defaultErrors: ['Die Kombination E-Mail/Passwort ist falsch.']
      },

      // define the api bend point to the server for the login (request)
      logout: {
        // must match the actual BE api bend point
        endpoint: '/auth/logout'
      }
    }),
  ],

  // some configs for the auth related forms
  forms: {
    // override some default configs
    login: {
      redirectDelay: 0, // do not need to wait after success I guess
      rememberMe: false, // we do not support that for now
      showMessages: {
        success: false // not needed as well, as we do not wait after success
      },
    },
    // can we define here our custom validators ????
    validation: {}
  }
};
