import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManagerComponent} from './manager.component';

/**
 * Routes within the manager module.
 */
const routes: Routes = [
  {
    path: '',
    component: ManagerComponent
  }
];

/**
 * Responsible for the routing in the manager module.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule {
}
