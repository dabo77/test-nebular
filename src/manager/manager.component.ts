import {Component} from '@angular/core';

@Component({
  templateUrl: 'manager.component.html',
  styleUrls: ['manager.component.scss']
})
export class ManagerComponent {

  /**
   * Profile menu items
   */
  profileMenuItems = [
    {
      title: 'Profile'
    },
    {
      title: 'Logout'
    }
  ];

  /**
   * Sidebar menu items
   */
  sidebarItems = [
    {
      title: 'Customers',
      expanded: true,
      children: [
        {
          title: 'Karlsruhe'
        },
        {
          title: 'Ettlingen'
        },
        {
          title: 'Rastatt'
        },
        {
          title: 'Baden-Baden'
        }
      ]
    },
    {
      title: 'Products',
      expanded: true,
      children: [
        {
          title: 'E-Ticket App'
        },
        {
          title: 'E-Ticket Scanner'
        },
        {
          title: 'Dashboard'
        }
      ]
    },
    {
      title: 'Contact Us'
    },
    {
      title: 'About'
    }
  ];
}
