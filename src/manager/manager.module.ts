import {NgModule} from '@angular/core';
import {ManagerComponent} from './manager.component';
import {ManagerRoutingModule} from './manager-routing.module';
import {SharedModule} from '../shared/shared.module';

/**
 * Responsible for the core functionality and process
 * as the manager is the core dashboard.
 */
@NgModule({
  declarations: [
    ManagerComponent
  ],
  imports: [
    ManagerRoutingModule,
    SharedModule
  ],
  exports: []
})
export class ManagerModule { }
