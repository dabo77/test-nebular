import {NgModule} from '@angular/core';
import {
  NbAlertModule,
  NbButtonModule, NbCardModule,
  NbCheckboxModule, NbContextMenuModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule, NbMenuModule,
  NbSidebarModule
} from '@nebular/theme';
import {FormsModule} from '@angular/forms';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {CommonModule} from '@angular/common';

/**
 * This module is responsible for other building blocks
 * such as directives, pipes, other modules (f.e. UI) etc.
 * that will be shared and used by other modules in the project.
 */
@NgModule({
  declarations: [],
  providers: [],
  imports: [
    FormsModule,
    NbAlertModule,
    NbCheckboxModule,
    NbInputModule,
    NbLayoutModule,
    NbSidebarModule.forRoot(),
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbMenuModule.forRoot(),
    NbContextMenuModule,
    NbCardModule,
    CommonModule
  ],
  exports: [
    FormsModule,
    NbAlertModule,
    NbCheckboxModule,
    NbInputModule,
    NbLayoutModule,
    NbSidebarModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbMenuModule,
    NbContextMenuModule,
    NbCardModule,
    CommonModule
  ]
})
export class SharedModule { }

